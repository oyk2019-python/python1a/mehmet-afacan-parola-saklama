parolalar = []	#parolalar adlı bir liste oluşturduk
while True:	#içerden aksi söylenmediği taktirde sonsuza kadar devam edecek bir while döngüsü yazdık sebebi de "True"
    print('Bir işlem seçin')	#kullanıcıya işlem seçmesini belirtiyoruz	
    print('1- Parolaları Listele')	#kullanıcıya 2 işlem seçeneği sunuluyor 1.si parolaları listelemek
    print('2- Yeni Parola Kaydet')	#2.si de yeni parola kaydetmek
    islem = input('Ne Yapmak İstiyorsun :')	#kullanıcıya ne yapmak istediğini soruyoruz ve 1 veya 2 girmesini bekliyoruz konsola
    if islem.isdigit():		#eğer kullanıcıdan aldığımız input yani islem sayı mı diye kontrol ediyoruz sayıysa alttaki işlemleri yapıyoruz
        islem_int = int(islem)		#islemdeki sayıyı integera çevirerek islem_int variableında tutuyoruz
        if islem_int not in [1, 2]:	#eğer islem_int 1 veya 2 mi diye kontrol ediyoruz
            print('Hatalı işlem girişi') #eğer islem_int 1 veya 2 değilse ekrana hatalı işlem diye bastırıyoruz
            continue	#eğer islem_int 1 veya 2 değilse continue diyerek döngünün başına dönüyoruz ve her şeye yeniden başlıyoruz
        if islem_int == 2: #eğer islem_int 2 ise kullanıcı yeni parola kaydet seçeneğini seçmiş oluyor ve biz de buna göre aşağıdakileri yapıyoruz

            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')	#kullanıcıdan girdi ismi isteyip girdi_ismi variableına atıyoruz

            kullanici_adi = input('Kullanici Adi Girin :')	#kullanıcıdan kullanıcı ismi isteyip kullanici_adi variableına atıyoruz

            parola = input('Parola :')		#kullanıcıdan parola isteyip parola variableına atıyoruz
            parola2 = input('Parola Yeniden :') 	#kullanıcıdan aynı parolayı kontrol amaçlı girmesini isteyip parola2 variableına atıyoruz
            eposta = input('Kayitli E-posta :')		#kullanıcıdan eposta isteyip eposta variableına atıyoruz
            gizlisorucevabi = input('Gizli Soru Cevabı :')	##kullanıcıdan gizli soru cevabı isteyip gizlisorucevabi variableına atıyoruz
            if kullanici_adi.strip() == '':	#kullanıcının kullanıcı adı girip girmediğini kontrol ediyoruz
                print('kullanici_adi girmediz') #kullanıcı kullanıcı adı girmediği taktirde kullanıcıya kullanıcı adı girmediğini söylüyoruz
                continue #kullanıcı kullanıcı adı girmediği taktirde döngüyü başa sarıyoruz
                
            if parola.strip() == '':	#kullanıcının parola girip girmediğini kontrol ediyoruz
                print('parola girmediz')	#kullanıcı parola girmediği taktirde kullanıcıya parola girmediğini söylüyoruz
                continue	#kullanıcı parola girmediği taktirde döngüyü başa sarıyoruz
                
            if parola2.strip() == '':	#kullanıcının kontrol amaçlı parolasını tekrar girip girmediğini kontrol ediyoruz
                print('parola2 girmediz') 	#kullanıcı parola2 girmediği taktirde kullanıcıya kontrol amaçlı olan parolayı girmediğini söylüyoruz
                continue	#kullanıcı parola2 girmediği taktirde döngüyü başa sarıyoruz
                
            if eposta.strip() == '':	#kullanıcının epostasını girip girmediğini kontrol ediyoruz
                print('eposta girmediz')	#kullanıcı eposta girmediği taktirde kullanıcıya eposta girmediğini söylüyoruz
                continue	#kullanıcı eposta girmediği taktirde döngüyü başa sarıyoruz
                
            if gizlisorucevabi.strip() == '':	#kullanıcının gizli soru cevabı girip girmediğini kontrol ediyoruz
                print('gizlisorucevabi girmediz')	#kullanıcı gizli soru girmediği taktirde kullanıcıya gizli soru cevabı girmediğini söylüyoruz
                continue 	#kullanıcı gizli soru girmediği taktirde döngüyü başa sarıyoruz
                
            if girdi_ismi.strip() == '':	#kullanıcının girdi ismi girip girmediğini kontrol ediyoruz
                print('Girdi ismi girmediz')	#kullanıcı girdi ismi girmediği taktirde kullanıcıya girdi ismi girmediğini söylüyoruz
                continue	#kullanıcı girdi ismi girmediği taktirde döngüyü başa sarıyoruz
                
            if parola2 != parola:	#kullanıcının parolasını ve kontrol amaçlı tekrar girilen parolanın aynı olup olmadığını kontrol ediyoruz
                print('Parolalar eşit değil') #parolalar eşit olmadığı taktirde bunu kullanıcıya bildiriyoruz
                continue	#parolalar eşit olmadığı taktirde döngüyü başa sarıyoruz

            yeni_girdi = {	#yeni_girdi isimli sözlük oluşturuyoruz girdi ismini kullanıcı adını parolayı epostayı gizli soru cevabını sözlüğe ekliyoruz
                'girdi_ismi': girdi_ismi,
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            parolalar.append(yeni_girdi)	#parolalar listesine yeni_girdi sözlüğümüzü ekliyoruz
            continue	#ve döngüyü başa sarıyoruz
            
        elif islem_int == 1: #yukardı kalan ifin devamı eğer işlemlerden 1i seçerse kullanıcı aşağıdakileri yapıyoruz
            alt_islem_parola_no = 0	#variable oluşturup 0a eşitliyoruz
            for parola in parolalar:	#parolalar listesindeki sözlükler içinde for döndürüyoruz ve her seferinde parola içinde oluyor bunlar
                alt_islem_parola_no += 1 #parolalar listesindeki sözlükler kadar alt_islem_parola_noyu arttırıyoruz teker teker
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi'))) #burda alt_islem_parola_noları bastırıyoruz teker teker parolalar listesindeki sözlük sayısı kadar yani yanına da listeki sözlüklerin girdi_isimlerini teker teker sırayla bastırıyoruz
            alt_islem = input('Yukarıdakilerden hangisi ?: ')  #alt_islem variableına yukarılardan hangisi diyerek input alıyoruz
            if alt_islem.isdigit():	#input sayı mı diye kontrol ediyoruz
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(alt_islem): 	   #alt_isleme kullanıcı tarafından girilen inputtaki sayı 1den küçük mü ve alt_islem parolalar listemizdeki eleman sayısı yani kütüphane sayısından 1 eksik mi diye kontrol ediyoruz
                    print('Hatalı parola seçimi')	#alt_islem sayısı parolalar listesinde herhangi bir kütüphaneye denk gelmediği taktirde hatalı parola seçimi diye bastırıyoruz
                    continue  	#alt_islem sayısı parolalar listesinde herhangi bir kütüphaneye denk gelmediği taktirde döngüyü başa sarıyoruz
                parola = parolalar[int(alt_islem) - 1] 		#parolayı kullanıcıdan aldığımız alt_islem sayısına göre denk gelen parolalar listemizdeki kütüphaneye eşitliyoruz
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format(	#az önce parolayı parolalar listesindeki uygun kütüphaneye ekledik ve şimdi de ordan bilgileri çekip bastırıyoruz
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue  #ve döngüyü başa sarıyoruz

    print('Hatalı giriş yaptınız') # en başta eğer sayı girmezsek üst tarafı atlayıp direk ekrana bunu basıyoruz
